
# 1) Connaitre la consistence de notre projet
	selt projet.ktz -f "-(<>(Fin /\ 16h37 /\ -retard))" -S q1.scn
# Réponse FALSE : Donc formule, satisfiable

	plan q1.scn projet.ndr > q1.plan



# 2)
selt projet.ktz -f "-(<>navette_eq /\ <>marche_pass)" -S q2.scn
#Réponse : FALSE : donc satisfiable

	plan q2.scn projet.ndr > q2.plan



	selt projet.ktz  -f "-((<>navette_eq /\ <>marche_pass)=> <> retard)" -S q22.scn

	plan q22.scn projet.ndr > q22.plan


	selt projet.ktz -f "-(<>retard /\ <>17h30_17h55)"
	selt projet.ktz -f "-(<>retard /\ <>18h30_18h55)"
	selt projet.ktz -f "-(<>retard /\ <>19h30_19h55)"
	selt projet.ktz -f "-(<>retard /\ <>20h30_20h55)"
